import mocks.MockBuilder;
import mocks.Produit;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ProduitServiceTest {


    private static List<Produit> produitList;
    private String password = "uDzCuHaNhZGtu4MZJuez7ShtnSt2K6b29vU93XbyGbrenZgwnp7Ex43UZLV3QAAF";
    private String login = "Gezabelle";
    @BeforeAll
    public static void init(){
        produitList = MockBuilder.getInstance().mockData();
    }
    @Test
    @DisplayName("Generic Data")
    public  void MockTest() {
        assertThat(produitList.size()).isEqualTo(50);
    }
}
