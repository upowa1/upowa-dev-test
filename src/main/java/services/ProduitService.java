package services;

import mocks.Produit;

import java.util.List;

public interface ProduitService {
    String auth(String login, String password);
    List<Produit> filter(List<Produit> produits);
}
